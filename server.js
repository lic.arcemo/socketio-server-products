var express = require('express');
var app     = express();
var server  = require('http').Server(app);
var io      = require('socket.io')(server);

var products = [
  {
    id      : 1,
    name    : 'TV samsung 42" 2018',
    price   : '$19,800.00',
    company   :'Samsung'
  },
  {
    id      : 2,
    name    : 'TV Sony 50" 2019',
    price   : '$27,900.00',
    company   :'Sony'
  }
];


io.on('connection', function(socket) {
	console.log('Un cliente se ha conectado');
    socket.emit('getProducts', products);

    socket.on('newProduct', (prod) => {
      products.push(prod);
      socket.broadcast.emit('addProduct', prod);
    });
});


server.listen(8080, function() {
	console.log('Servidor corriendo en http://localhost:8080');
});
